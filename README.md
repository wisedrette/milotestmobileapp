#Milo Solutions Test Task

29 Feb 2016

This project was created as a part of an application process to Milo Solutions.
Project represents simple mobile application created with ionic2 framework and a few Cordova plugins. The application's functionality has been extended with facility to configure a 'Reset' option. So, it's possible to reset either name, or photo, or both name and photo according to settings. The default setting is resetting everything.

Only the version built with ionic2 is present cause similar frameworks for ReactJS have several caveats:

- Reapp framework is currently broken, as not all components of this framework are migrated from React@0.13.x to React@0.14.x

- TouchStoneJS is a workable solution, but currently has no documentation at all, only sample project on Git.

- ReactNative is a workable solution as well, has a good documentation, but requires a lot more understanding of Android Core to start developing with. 



This application has been tested on real device with Android vers. 5.1.1 and on emulated devices with Android versions 5.1.0, 4.4.4, 4.1.1.

Here are links for .apk builds for android-5.0.1 and android-4.1.1:

android-release-4.1.1.apk - https://drive.google.com/file/d/0B8uuIPhDqkh3RVNoQTdoVHFkamc/view?usp=sharing

android-release-5.1.0.apk - https://drive.google.com/file/d/0B8uuIPhDqkh3Uzlrc1BySS1scWM/view?usp=sharing

Those builds differ not only by android version, but by the SplashScreen plugin for Cordova - the 5.1.0 build has additional animation during default welcome screen.

#### In order to make the application work, ionic2 framework and cordova are required. Following commands could be used for installation: 
$ npm install -g ionic@beta

$ npm install -g cordova

#### To launch this application in browser, you need:
$ git clone git@bitbucket.org:wisedrette/milotestmobileapp.git

$ cd MiloTestMobileApp

$ npm install

$ ionic serve

#### To build .apk for this aplication:

$ ionic platform add android

or, for android 4.1.1:
$ ionic platform add android@4.1.1

please notice, that only one android platform could be installed at the same moment

$ ionic build android

Generated .apk file will be located in ./platforms/android/build/outputs/apk/ folder

##### Camera and Splashscreen Cordova plugins should be installed during npm install according to dependencies in package.json. However, these plugins could be installed separately as well:

$ ionic plugin add cordova-plugin-camera

Splashscreen plugin could be installed in two ways:

$ ionic plugin add cordova-plugin-splashscreen
or
$ cordova plugin add https://github.com/apache/cordova-plugin-splashscreen.git

In case of using the second installation variant of this plugin, splashscreen will be represented not as static image only but with some animation as well.