import {App, Alert, IonicApp, Platform, Storage, LocalStorage, MenuController} from 'ionic-framework/ionic';
import {HomePage} from './pages/home/home';
import {CameraPage} from './pages/camera/camera';
import {ComplimentPage} from './pages/compliment/compliment';

//used for RadioButtons in Reset Settings side menu
import {
    Control,
    ControlGroup,
    NgForm,
    Validators,
    NgControl,
    ControlValueAccessor,
    NgControlName,
    NgFormModel,
    FormBuilder
} from 'angular2/common';


@App({
    templateUrl: 'build/app.html',
    config: {}
})
class MyApp {
    static get parameters() {
        return [[IonicApp], [Platform], [MenuController]];
    }

    constructor(app, platform, menu) {
        // set up app
        this.app = app;
        this.platform = platform;
        this.menu = menu;
        this.local = new Storage(LocalStorage);
        this.initializeApp();

        // configure reset settings ngControls for side menu
        this.settings = new Control("all");
        if (this.local.get('settings')._result == null)
            this.local.set('settings', 'all');
        this.settingForm = new ControlGroup({
            "settings": this.settings
        });

        // define which page should be a root page according to saved data in LocalStorage
        let hasImage = this.local.get('image')._result != null;
        let hasName = this.local.get('name')._result != null;
        if (hasName && hasImage){
            this.rootPage = ComplimentPage;
        } else if (hasName){
            this.rootPage = CameraPage;
        } else {
            this.rootPage = HomePage;
        }
    }

    initializeApp() {
        this.platform.ready().then(() => {
            if (window.StatusBar) {
                window.StatusBar.styleDefault();
            }
        });
    }

    doSubmit(event) {
        this.local.set('settings', this.settingForm.value.settings);
        event.preventDefault();

        this.menu.close();
    }
}
