import {Page, NavController, NavParams, Platform, Storage, LocalStorage} from 'ionic-framework/ionic';
import {NgZone} from 'angular2/core';
import {ComplimentPage} from '../compliment/compliment';

@Page({
    templateUrl: 'build/pages/camera/camera.html'
})
export class CameraPage {
    static get parameters() {
        return [[NavController], [NavParams], [Platform], [NgZone]];
    }

    constructor(nav, navParams, platform, _zone) {
        this.nav = nav;
        this._zone = _zone;
        this.platform = platform;
        this.images = [];
        this.local = new Storage(LocalStorage);

        // If we navigated to this page, we will have an item available as a nav param
        this.username = navParams.get('name');
        if (this.username == undefined)
            this.username = this.local.get('name')._result;
    }

    takePhoto() {
        // additional check of Cordova plugin availability, used for development and navigation in browser
        if (navigator.camera === undefined) {
            this.local.set('image', 'img/appicon.png');
            this.nav.setRoot(ComplimentPage);
        } else {

            this.platform.ready().then(() => {
                let options = {
                    quality: 80,
                    allowEdit: false,
                    saveToPhotoAlbum: false
                };


                navigator.camera.getPicture(
                    (data) => {
                        this.local.set('image', data);

                        // navigation is proceeded only when controls have been returned from camera
                        this._zone.run(()=>
                            this.nav.setRoot(ComplimentPage)
                        )
                    }, (error) => {
                        alert(error);
                    }, options
                );

            });
        }
    }
}