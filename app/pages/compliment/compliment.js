import {Page, NavController, NavParams, Storage, LocalStorage} from 'ionic-framework/ionic';
import {HomePage} from '../home/home';
import {CameraPage} from '../camera/camera';

@Page({
    templateUrl: 'build/pages/compliment/compliment.html'
})
export class ComplimentPage {
    static get parameters() {
        return [[NavController]];
    }

    constructor(nav) {
        this.nav = nav;
        this.local = new Storage(LocalStorage);

        // Getting data from local storage
        this.image = this.local.get('image')._result;
        this.username = this.local.get('name')._result;
    }

    reset(event) {
        let resetSetting = this.local.get('settings')._result;

        switch (resetSetting) {
            case 'name':
                this.local.remove('name');
                this.nav.setRoot(HomePage);
                break;

            case 'photo':
                this.local.remove('image');
                this.nav.setRoot(CameraPage);
                break;

            default:
                this.local.remove('name');
                this.local.remove('image');
                this.nav.setRoot(HomePage);
        }
    }
}