import {Page, NavController, NavParams, Storage, LocalStorage} from 'ionic-framework/ionic';
import {CameraPage} from '../camera/camera';
import {ComplimentPage} from '../compliment/compliment';

@Page({
    templateUrl: 'build/pages/home/home.html'
})
export class HomePage {
    static get parameters() {
        return [[NavController], [NavParams]];
    }

    constructor(nav) {
        this.nav = nav;
        this.local = new Storage(LocalStorage);
        this.username = '';
    }

    openNextPage(event) {
        this.local.set('name', this.username);
        if (this.local.get('image')._result != null) {
            this.nav.setRoot(ComplimentPage);
        } else {
            // used push instead of setRoot in order to get 'back' button
            this.nav.push(CameraPage, {
                name: this.username
            });
        }
    }
}
